#' Visualisation des palette
#'
#' @param pal une palette definie comme un vecteur de couleurs nommees
#'
#' @return un plot
#' @importFrom stringr str_wrap
#' @importFrom graphics barplot
#' @export
#'
#' @examples
#' pal_typ_valo <- c("injection" = "#FF6F4C", "cogénération" = "#5770BE")
#' viz_pal(pal_typ_valo)

viz_pal <- function(pal) {
  barplot(rep(1, length(pal)), names.arg = stringr::str_wrap(names(pal), 3),
          col = pal , border = NA, axes = FALSE, horiz = FALSE, cex.names = 0.6)
}
